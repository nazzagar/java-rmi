# IVH5 Java RMI code #

This repository contains example Java RMI files.

## Requirements ##
To run any Java RMI application in this project a webserver is required. The webserver enables the up- and downloading of Java classes between client and server. Without this, the client and serer cannot communicate.

The Maven buildscripts create a directory with the name 'C:\dev\workspace\worked-example\IVH5\target'. Upon building, Maven copies the required classes, libraries, resources, security policy files and property files to this directory. The webserver that you use must be setup to serve documents from the document root 'localhost/classes'. The easies way to do that is to install XAMPP in C:\dev\xampp. 

When the webserver has been setup and started, and `mvn package` has been run, you should be able to view the classes via 'localhost/classes' in any webbrowser.

## Usage ##
1. To get this code, fork it to your own repo and clone it to your desktop.
2. To build the code, install Maven.
3. From a command prompt in the examples root directory, run `mvn package`.

## Running the examples##
To run the Java RMI examples, do the following:

1. Start Apache and make sure that directory 'localhost/classes' can be found by typing this in the address bar of your webbrowser.
2. Run the batchfile `start_rmiregistry.bat`. Note that the codebase parameter sets up the directory where the registry will serve the required class files. If the classes cannot be found in the codebase directory, the server will not be able to register its service in the registry.
3. Run the example files.
	
## Maven commands ##

| Command | Actie                    |
| -------------------------- | ---------------------------------- |
| `mvn package`      | Compile, build and test code.   |
| `mvn clean` | Remove all generated classfiles. |

